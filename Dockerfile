FROM golang:1.8
WORKDIR /app
RUN apt update
RUN apt install curl git -y -qq
WORKDIR $GOPATH/src  
RUN go get -u github.com/mholt/caddy  
RUN go get -u github.com/caddyserver/builds
RUN go get -u github.com/caddyserver/dnsproviders/dnsimple
WORKDIR $GOPATH/src/github.com/mholt/caddy/caddy
RUN sed -i '/\/\/ This is where other plugins get plugged in (imported)/c\_ "github.com/caddyserver/dnsproviders/dnsimple"' ./caddymain/run.go
#sed -i '/TEXT_TO_BE_REPLACED/c\This line is removed by the admin.' /tmp/foo
RUN go run build.go -goos=linux -goarch=amd64
RUN mv caddy /usr/local/bin
VOLUME /etc/ssl/caddy


FROM debian:buster-slim
WORKDIR /app
RUN apt-get update  
RUN apt-get install -y ca-certificates
COPY --from=0 /usr/local/bin/caddy /usr/local/bin/caddy
ADD ./run.sh /app
RUN chmod +x /app/run.sh
CMD "./run.sh"




