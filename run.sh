#!/bin/bash
echo "${CADDYFILE}" | sed 's/;/\n/g' > /app/Caddyfile
caddy -conf /app/Caddyfile -log stdout -agree
